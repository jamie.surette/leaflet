var arcgisOnline = L.esri.Geocoding.arcgisOnlineProvider();
// create the geocoding control and add it to the map
var searchControl = L.esri.Geocoding.geosearch({
    providers: [arcgisOnline]
}).addTo(map);
// create an empty layer group to store the results and add it to the map
var results = L.layerGroup().addTo(map);
// listen for the results event and add every result to the map
searchControl.on("results", function(data) {
    results.clearLayers();
    for (var i = data.results.length - 1; i >= 0; i--) {
        results.addLayer(L.marker(data.results[i].latlng));
    }
});
