// AGREGAR MAPA
var map = L.map('map', {
    fullscreenControl: true,
    center: [35.962, -5.665],
    layers: [mapBoxLayerDark],
    zoom: 5,
    minZoom: 2,
    maxZoom: 18,
    pseudoFullscreen: false,
});

// mapas base
var baseMaps = {
    "Google Roads": roads,
    "Google Satellite": satellite,
    "Google Hybrid": hybrid,
    "Google Terrain": terrain,
    "MapBox Streets": mapBoxLayerStreets,
    "MapBox Streets Satellite": mapBoxLayerStreetsSatellite,
    "MapBox Light": mapBoxLayer,
    "MapBox Dark": mapBoxLayerDark,
    "MapBox Open Street Map": osm,
};

L.control.groupedLayers(baseMaps).addTo(map); // agregar mapas
