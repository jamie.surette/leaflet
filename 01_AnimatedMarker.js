// ruta
var polilinea = L.polyline([
    [40.9650, -5.6640],
    [40.96452, -5.66476],
    [40.96399, -5.66466],
    [40.9612, -5.66662],
    [40.9604, -5.6672],
    [40.96063, -5.66792],
    [40.96245, -5.66663],
    [40.96266, -5.66593]
], {
    color: "red"
});

var ruta = L.polyline([
  [ 40.9655,  -5.6639],
  [ 40.9664,  -5.6640],
  [ 40.9664,  -5.6635],
  [ 40.9662,  -5.6629],
  [ 40.9662,  -5.6629],
  [ 40.9654,  -5.6626],
  [ 40.9664,  -5.6614],
  [ 40.9669,  -5.6614]
],{
  color: "red",
}).addTo(map).bindPopup("Ruta Optima.");

//Creamos iconos
var LeafIcon = L.Icon.extend({
    options: {
        iconSize: [30, 30],
        iconAnchor: [2, 9],
        popupAnchor: [-3, -7]
    }
});
var peaton = new LeafIcon({
    iconUrl: 'images/peaton.gif'
});

// abrir un popup al terminar la animación
var fin = function popup() {
  L.popup().setLatLng([ 40.9669,  -5.6614]).setContent(
    `<ul>
      <h3>Ruta optima de visita</h3>
      <li>Salimos del ayuntamiento hacia el Colegio Mayor Montellano </li>
      <li>Giramos en la calle especias</li>
      <li>Cruzamos por el restaturante</li>
    </ul>`).addTo(map);
};

// capa del marcador
var animatedMarker = L.animatedMarker(ruta.getLatLngs(), {
    autoStart: false,
    icon: peaton,
    distance: 500, // metros
    interval: 3000, // milisegundos
    onEnd: fin,
});

// iniciar y parar recorrido del marcador
function start() {
    animatedMarker.start();
}

function stop() {
    animatedMarker.stop();
}

// agregar marcador animado al mapa
map.addLayer(animatedMarker);
