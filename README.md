### Este proyecto puede visualizarse con servidor ligero como:

Python: [Using Python HttpServer as a simple HTTP Server
](https://www.askpython.com/python-modules/python-httpserver) 

`python -m SimpleHTTPServer`

Node: [Live Server](https://www.npmjs.com/package/live-server)

`npm install -g live-server`

`ive-server`