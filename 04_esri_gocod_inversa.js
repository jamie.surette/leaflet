var geocodeService = L.esri.Geocoding.geocodeService();
// map.on('click', function(e) {
//     geocodeService.reverse().latlng(e.latlng).run(function(error, result) {
//         L.marker(result.latlng).addTo(map).bindPopup(result.address.Match_addr).openPopup();
//     });
// });


var r = L.marker();
map.on('click', function(e) {
  
    geocodeService.reverse().latlng(e.latlng).run(function(error, result) {
        map.removeLayer(r);
        r = L.marker(result.latlng).addTo(map).bindPopup(result.address.Match_addr).openPopup();
    });
});
