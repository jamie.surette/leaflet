var map = L.map('map').setView([40.965, -5.664], 16);
var osm = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);

var options = {
    data: {
        'España': 60,
        'Europa': 25,
        'America': 10,
        'Resto': 5
    },
    chartOptions: {
        'España': {
            fillColor: 'blue',
            maxHeight: 50,
            maxValue: 60,
            minValue: 0,
        },
        'Europa': {
            fillColor: 'red',
            maxHeight: 50,
            maxValue: 60,
            minValue: 0,
        },
        'America': {
            fillColor: 'green',
            minValue: 0,
        },
        maxValue: 60,
        maxHeight: 50,
        'Resto': {
            fillColor: 'yellow',
            minValue: 0,
            maxValue: 60,
            maxHeight: 50,
        }
    },
    weight: 1,
    color: '#000000',
    radius: 30,
    fillOpacity: 1
};

var barras = new L.BarChartMarker([40.9657, -5.662], options);
map.addLayer(barras);

var tarta = new L.PieChartMarker([40.9623, -5.6666], options);
map.addLayer(tarta);
