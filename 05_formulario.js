var geocodeService = L.esri.Geocoding.geocodeService();

var r = L.marker();
map.on('click', function(e) {
    geocodeService.reverse().latlng(e.latlng).run(function(error, result) {
        map.removeLayer(r);
        r = L.marker(result.latlng).addTo(map).bindPopup(result.address.Match_addr).openPopup();
    });
});

function myFunction() {
    var calle = document.getElementById("calle").value;
    var numero = document.getElementById("numero").value;
    var ciudad = document.getElementById("ciudad").value;
    var pais = document.getElementById("pais").value;
    var url = document.URL;
    var nuevaUrl = document.getElementById("demo").innerHTML = url + "?a=" + calle + "," + numero + "," + ciudad + "," + pais;
    window.location = nuevaUrl;
}

function reset() {
    window.location.assign("file:///Users/angaven/Library/ApplicationSupport/GeoServer/jetty/webapps/geoserver/www/curso_leaflet/URLgeo_codeFormulario.html");
}
